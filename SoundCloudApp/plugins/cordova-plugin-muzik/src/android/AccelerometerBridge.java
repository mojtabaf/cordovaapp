package com.muzik.androidplugin;

import com.muzik.accessory.MzAccessory;
import com.muzik.accessory.callback.IAccelerometerCallback;

import java.util.LinkedList;
import java.util.Queue;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

public class AccelerometerBridge implements IAccelerometerCallback {

    private MzAccessory mzAccessory;
    private Queue<CallbackContext> oneTimeCallbackQueue;
    private CallbackContext intervalCallback;

    public AccelerometerBridge(MzAccessory mzAccessory) {
        this.mzAccessory = mzAccessory;
        this.oneTimeCallbackQueue = new LinkedList<CallbackContext>();
    }

    public synchronized void getAccelerometerSample(CallbackContext callback) {
        this.oneTimeCallbackQueue.add(callback);
        this.mzAccessory.getAccelerometerSample(this);
    }

    public synchronized void registerForAccelerometerDataStream(CallbackContext callback) {
        this.intervalCallback = callback;
        this.mzAccessory.registerForAccelerometerDataStream(this);
    }

    public synchronized void unregisterForAccelerometerDataStream() {
        if (this.intervalCallback != null) {
            this.intervalCallback = null;
            this.mzAccessory.unregisterForAccelerometerDataStream();
        }
    }
    
    @Override
    public synchronized void onResponseReceived(float x, float y, float z, float norm, float forwardAngle, float sideAngle) {
        JSONArray response = new JSONArray();
        try {
            response.put((double) x);
            response.put((double) y);
            response.put((double) z);
            response.put((double) norm);
            response.put((double) forwardAngle);
            response.put((double) sideAngle);
        } catch (JSONException e) { }

        if (!this.oneTimeCallbackQueue.isEmpty()) {
            this.oneTimeCallbackQueue.poll().success(response);
        }

        if (this.intervalCallback != null) {
            PluginResult result = new PluginResult(PluginResult.Status.OK, response);
            result.setKeepCallback(true);
            this.intervalCallback.sendPluginResult(result);
        }
    }
}
