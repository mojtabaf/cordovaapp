"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ionic_angular_1 = require('ionic-angular');
var item_details_1 = require('../item-details/item-details');
var userService_1 = require('../../services/userService');
var trackService_1 = require('../../services/trackService');
require('rxjs/Rx');
var http_1 = require('angular2/http');
var ListPage = (function () {
    function ListPage(nav, navParams, http, trackService, userService) {
        var _this = this;
        this.nav = nav;
        this.trackService = trackService;
        this.userService = userService;
        // If we navigated to this page, we will have an item available as a nav param
        this.items = [];
        this.selectedItem = navParams.get('item');
        this.userService.getUserInfo().subscribe(function (user) {
            _this.user = user;
            trackService.getTracks(_this.user.id).subscribe(function (tracks) {
                _this.items = tracks;
            });
        });
        //http.get("https://api.soundcloud.com/resolve?url=https%3A//soundcloud.com/spadgos&client_id=7b98197aa405d7721e64fd3fb5b5eb5a")
        //    .map((res: Response) => res.json()).subscribe(res => {
        //        this.user = res;
        //        http.get("http://api.soundcloud.com/users/" + this.user.id + "/tracks?client_id=7b98197aa405d7721e64fd3fb5b5eb5a")
        //            .map((response: Response) => response.json()).subscribe(response => {
        //                this.items = response;
        //            });
        //        //this.items.push(res);
        //    });
        //var user = UserService.getUserInfo().subscribe((user) =>
        //{
        //    this.user = user;
        //    this.items.push(user);
        //   // var tracks = TrackService.getTracks(user.id).subscribe((tracks) => this.items = tracks)
        //}
        //);
        //get http://api.soundcloud.com/tracks/13158665?client_id=YOUR_CLIENT_ID"
        //SC.get("/tracks", {
        //    user_id: this.user.id,
        //    limit: 100
        //}, function (tracks: Array<Track>) {
        //    for (let track of tracks) {
        //        this.items.push({
        //            title: track.title,
        //            duration: track.duration
        //        });
        //    }});
        //get 
        //this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
        //'american-football', 'boat', 'bluetooth', 'build'];
        //this.items = [];
        //for(let i = 1; i < 11; i++) {
        //  this.items.push({
        //    title: 'Item ' + i,
        //    note: 'This is item #' + i,
        //    icon: this.icons[Math.floor(Math.random() * this.icons.length)]
        //  });
        //}
    }
    ListPage.prototype.itemTapped = function (event, item) {
        this.nav.push(item_details_1.ItemDetailsPage, {
            item: item
        });
    };
    ListPage = __decorate([
        ionic_angular_1.Page({
            templateUrl: 'build/pages/list/list.html', providers: [trackService_1.TrackService, userService_1.UserService]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, ionic_angular_1.NavParams, http_1.Http, trackService_1.TrackService, userService_1.UserService])
    ], ListPage);
    return ListPage;
}());
exports.ListPage = ListPage;
