import {Page, NavController, NavParams} from 'ionic-angular';
import {ItemDetailsPage} from '../item-details/item-details';
import {UserService} from '../../services/userService';
import {TrackService} from '../../services/trackService';
import {User} from '../../model/user';
import 'rxjs/Rx';
import {Track} from '../../model/track';
import {Http, Response} from 'angular2/http';


@Page({
    templateUrl: 'build/pages/list/list.html', providers: [TrackService, UserService] 
})

export class ListPage {
  selectedItem: any;
  icons: string[];
  items: Array<Track>;
  user: User;

  constructor(private nav: NavController, navParams: NavParams, http: Http, private trackService: TrackService, private userService: UserService) {
    // If we navigated to this page, we will have an item available as a nav param
      this.items = [];
      this.selectedItem = navParams.get('item');

     this.userService.getUserInfo().subscribe(user => {
         this.user = user;

         trackService.getTracks(this.user.id).subscribe(tracks => {
             this.items = tracks;
         });
     });

    
      //http.get("https://api.soundcloud.com/resolve?url=https%3A//soundcloud.com/spadgos&client_id=7b98197aa405d7721e64fd3fb5b5eb5a")
      //    .map((res: Response) => res.json()).subscribe(res => {
      //        this.user = res;
      //        http.get("http://api.soundcloud.com/users/" + this.user.id + "/tracks?client_id=7b98197aa405d7721e64fd3fb5b5eb5a")
      //            .map((response: Response) => response.json()).subscribe(response => {
      //                this.items = response;
      //            });
      //        //this.items.push(res);
      //    });
     
      //var user = UserService.getUserInfo().subscribe((user) =>
      //{
      //    this.user = user;
      //    this.items.push(user);

      //   // var tracks = TrackService.getTracks(user.id).subscribe((tracks) => this.items = tracks)
      //}
      //);
      //get http://api.soundcloud.com/tracks/13158665?client_id=YOUR_CLIENT_ID"
      //SC.get("/tracks", {
      //    user_id: this.user.id,
      //    limit: 100
      //}, function (tracks: Array<Track>) {
      //    for (let track of tracks) {
      //        this.items.push({
      //            title: track.title,
      //            duration: track.duration
      //        });
      //    }});
    //get 

    //this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    //'american-football', 'boat', 'bluetooth', 'build'];

    //this.items = [];
    //for(let i = 1; i < 11; i++) {
    //  this.items.push({
    //    title: 'Item ' + i,
    //    note: 'This is item #' + i,
    //    icon: this.icons[Math.floor(Math.random() * this.icons.length)]
    //  });
    //}
  }
  getVoiceCommand() {
      alert("recording....");
  }

  itemTapped(event, item) {
    this.nav.push(ItemDetailsPage, {
      item: item
    });
  }
}
