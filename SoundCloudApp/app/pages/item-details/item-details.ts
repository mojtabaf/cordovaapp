import {Page, NavController, NavParams} from 'ionic-angular';
declare var cordova: any;
declare var muzik: any;


@Page({
  templateUrl: 'build/pages/item-details/item-details.html'
})
export class ItemDetailsPage {
  selectedItem: any;
  src: string;
  constructor(private nav: NavController, navParams: NavParams) {
    // If we navigated to this page, we will have an item available as a nav param
      this.selectedItem = navParams.get('item');
      this.src = "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/" + this.selectedItem.id + "&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=false&amp;show_user=false&amp;show_reposts=false;sharing=true";
  }
  getInfo() {
      var geter = function (serial: string) {
          alert(serial);
      }
      muzik.getBluetoothLocalName(geter);
  }
}
