"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ionic_angular_1 = require('ionic-angular');
var ItemDetailsPage = (function () {
    function ItemDetailsPage(nav, navParams) {
        this.nav = nav;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        this.src = "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/" + this.selectedItem.id + "&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=false&amp;show_user=false&amp;show_reposts=false;sharing=true";
    }
    ItemDetailsPage.prototype.getInfo = function () {
        var geter = function (serial) {
            alert(serial);
        };
        muzik.getBluetoothLocalName(geter);
    };
    ItemDetailsPage = __decorate([
        ionic_angular_1.Page({
            templateUrl: 'build/pages/item-details/item-details.html'
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, ionic_angular_1.NavParams])
    ], ItemDetailsPage);
    return ItemDetailsPage;
}());
exports.ItemDetailsPage = ItemDetailsPage;
