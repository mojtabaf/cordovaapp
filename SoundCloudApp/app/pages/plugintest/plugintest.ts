﻿import {Page} from 'ionic-angular';
import {PluginService} from '../../services/pluginService';
declare var muzik;

@Page({
    templateUrl: 'build/pages/plugintest/test.html', providers: [PluginService]
})
export class PluginTest {
    pressedButton: string;
    constructor(private pluginService: PluginService) {
        //var service = this.pluginService;
        //this.sdkRegistration(function () {
        //    //alert(connectionState);
        //    service.registerForConnectionState(function (connectionState) {
        //        if (connectionState == muzik.CONNECTION_STATE.CONNECTED){
        //            service.registerForAllGestures(function (data) { alert(data) });
        //        }
        //    });
        //});
        
        
        //
        //connectionServic;

        //this.pluginService.startServer();
        //this.pluginService.registerForAllGestures(function (data) { alert(data); });
        //this.pluginService.startServer()
        //    .then(this.pluginService.registerForConnectionState(function (data) { alert(data) }));
        //var service = this.pluginService;
        //this.sdkRegistration(function () {
        //    service.registerForConnectionState();
        //    service.registerForAllGestures();
        //});
        this.pressedButton = "mojtaba";
    }
    startServer() {
        this.pluginService.startServer();
    }
    stopServer() {
        this.pluginService.stopServer();
    }
    getBlueToothLocalName() {
        var data = this.pluginService.getBluetoothLocalName();
        //alert(data);
    }
    getConnectionState() {
        //this.pluginService.registerForConnectionState();
    }
    getBatteryLevel() {
        this.pluginService.getBatteryLevel();
    }
    getBluetoothLocalName() {
        this.pluginService.getBluetoothLocalName();
    }
    //setBluetoothLocalName(name: string) {
    //    this.pluginService.setBluetoothLocalName(name);
    //}
    getChargeStatus() {
        this.pluginService.getChargeStatus();
    }
    getSerialNumber() {
        this.pluginService.getSerialNumber();
    }
    getManufacturer() {
        this.pluginService.getManufacturer();
    }
    getFirmwareVersion() {
        this.pluginService.getFirmwareVersion();
    }
    getHardwareVersion() {
        this.pluginService.getHardwareVersion();
    }
    getMspVersion() {
        this.pluginService.getMspVersion();
    }
    getLibraryVersion() {
        this.pluginService.getLibraryVersion();
    }
    registerForAllGestures() {
        var service = this.pluginService;
        this.sdkRegistration(function () {
            //alert(connectionState);
            service.registerForConnectionState(function (connectionState) {
                if (connectionState == muzik.CONNECTION_STATE.CONNECTED) {
                    service.registerForAllGestures(function (data) { alert(data) });
                }
            });
        });
        //this.pluginService.registerForAllGestures(function (data) { alert(data) });
    }
    unregisterForAllGestures() {
        this.pluginService.unregisterForAllGestures();
    }
    registerForGesture() {
        var service = this.pluginService;
        this.sdkRegistration(function () {
            //alert(connectionState);
            service.registerForConnectionState(function (connectionState) {
                if (connectionState == muzik.CONNECTION_STATE.CONNECTED) {
                    muzik.registerForGestures(function (data) { alert(data) }, 3, 5);
                }
            });
        });
        
    }
    private sdkRegistration(callback) {
        this.pluginService.startServer();
        callback();
        //var service = this.pluginService;
        //var cb = function () {
        //    service.registerForConnectionState();
        //}
        //var starting = function (callback) {
        //    service.startServer();
        //    callback();
        //};
        //starting(cb);
    }
}