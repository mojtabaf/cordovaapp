"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ionic_angular_1 = require('ionic-angular');
var pluginService_1 = require('../../services/pluginService');
var PluginTest = (function () {
    function PluginTest(pluginService) {
        //var service = this.pluginService;
        //this.sdkRegistration(function () {
        //    //alert(connectionState);
        //    service.registerForConnectionState(function (connectionState) {
        //        if (connectionState == muzik.CONNECTION_STATE.CONNECTED){
        //            service.registerForAllGestures(function (data) { alert(data) });
        //        }
        //    });
        //});
        this.pluginService = pluginService;
        //
        //connectionServic;
        //this.pluginService.startServer();
        //this.pluginService.registerForAllGestures(function (data) { alert(data); });
        //this.pluginService.startServer()
        //    .then(this.pluginService.registerForConnectionState(function (data) { alert(data) }));
        //var service = this.pluginService;
        //this.sdkRegistration(function () {
        //    service.registerForConnectionState();
        //    service.registerForAllGestures();
        //});
        this.pressedButton = "mojtaba";
    }
    PluginTest.prototype.startServer = function () {
        this.pluginService.startServer();
    };
    PluginTest.prototype.stopServer = function () {
        this.pluginService.stopServer();
    };
    PluginTest.prototype.getBlueToothLocalName = function () {
        var data = this.pluginService.getBluetoothLocalName();
        //alert(data);
    };
    PluginTest.prototype.getConnectionState = function () {
        //this.pluginService.registerForConnectionState();
    };
    PluginTest.prototype.getBatteryLevel = function () {
        this.pluginService.getBatteryLevel();
    };
    PluginTest.prototype.getBluetoothLocalName = function () {
        this.pluginService.getBluetoothLocalName();
    };
    //setBluetoothLocalName(name: string) {
    //    this.pluginService.setBluetoothLocalName(name);
    //}
    PluginTest.prototype.getChargeStatus = function () {
        this.pluginService.getChargeStatus();
    };
    PluginTest.prototype.getSerialNumber = function () {
        this.pluginService.getSerialNumber();
    };
    PluginTest.prototype.getManufacturer = function () {
        this.pluginService.getManufacturer();
    };
    PluginTest.prototype.getFirmwareVersion = function () {
        this.pluginService.getFirmwareVersion();
    };
    PluginTest.prototype.getHardwareVersion = function () {
        this.pluginService.getHardwareVersion();
    };
    PluginTest.prototype.getMspVersion = function () {
        this.pluginService.getMspVersion();
    };
    PluginTest.prototype.getLibraryVersion = function () {
        this.pluginService.getLibraryVersion();
    };
    PluginTest.prototype.registerForAllGestures = function () {
        var service = this.pluginService;
        this.sdkRegistration(function () {
            //alert(connectionState);
            service.registerForConnectionState(function (connectionState) {
                if (connectionState == muzik.CONNECTION_STATE.CONNECTED) {
                    service.registerForAllGestures(function (data) { alert(data); });
                }
            });
        });
        //this.pluginService.registerForAllGestures(function (data) { alert(data) });
    };
    PluginTest.prototype.unregisterForAllGestures = function () {
        this.pluginService.unregisterForAllGestures();
    };
    PluginTest.prototype.registerForGesture = function () {
        var service = this.pluginService;
        this.sdkRegistration(function () {
            //alert(connectionState);
            service.registerForConnectionState(function (connectionState) {
                if (connectionState == muzik.CONNECTION_STATE.CONNECTED) {
                    muzik.registerForGestures(function (data) { alert(data); }, 3, 5);
                }
            });
        });
    };
    PluginTest.prototype.sdkRegistration = function (callback) {
        this.pluginService.startServer();
        callback();
        //var service = this.pluginService;
        //var cb = function () {
        //    service.registerForConnectionState();
        //}
        //var starting = function (callback) {
        //    service.startServer();
        //    callback();
        //};
        //starting(cb);
    };
    PluginTest = __decorate([
        ionic_angular_1.Page({
            templateUrl: 'build/pages/plugintest/test.html', providers: [pluginService_1.PluginService]
        }), 
        __metadata('design:paramtypes', [pluginService_1.PluginService])
    ], PluginTest);
    return PluginTest;
}());
exports.PluginTest = PluginTest;
