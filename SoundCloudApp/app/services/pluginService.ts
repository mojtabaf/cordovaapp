﻿import {Injectable} from 'angular2/core';
import {Observable} from 'rxjs/Rx';
declare var cordova: any;
declare var muzik: any;

@Injectable()
export class PluginService {
    constructor() {
        
    }
    startServer() {
        muzik.startServer();
    }
    stopServer() {
        muzik.stopServer();
    }
    registerForConnectionState(callback) {
        muzik.registerForConnectionState(callback);
    }
    observableStartServer()  {
        var server = this;
        let start = Observable.create(function (observer) {
            server.startServer();
            observer.onCompleted();
        });
        let connectionState = Observable.create(function (observer) {
            muzik.registerForConnectionState(function (data) {
                observer.onNext(data);
                observer.onCompleted();
            });
        });
        start.concat(connectionState).subscribe(function (data) { alert(data) });
    }
    observableConnectionState() {
       // var server = this;
        
    }

    registerForGestures(callback,...gestures: number[]) {
       
        // var cb = function () { };
        muzik.registerForGestures(callback, gestures);
    }
    registerForAllGestures(callback) {
        muzik.registerForAllGestures(callback, true);
        //this.startServer().then(() => {
        //    var result = muzik.registerForConnectionState(function (connectionState) {
        //        if (connectionState === "CONNECTED") {
        //            muzik.registerForAllGestures(callback, true);
        //        }
        //    })
        //});
    }
    unregisterForGestures() {
        muzik.registerForConnectionState(function (connectionState) {
            if (connectionState === "CONNECTED") {
                muzik.registerForAllGestures(true);
            }
        });
    }
    unregisterForAllGestures() {
        muzik.unregisterForAllGestures(true);
    }
    registerForMotion() {
        //TODO
    }
    unregisterForMotion() {
        //TODO
    }
    getAccelerometerSample() {
        //TODO
    }
    registerForAccelerometerDataStream() {
        //TODO
    }
    unregisterForAccelerometerDataStream() {
        //TODO
    }
    getBatteryLevel() {
    // meghdare battry goshio mide 
        muzik.getBatteryLevel(function (data) { alert(data) });
    }
    getBluetoothLocalName() {
        //esme bluethooth name marboot be headphono mide
        muzik.getBluetoothLocalName(function (data) {  alert(data) });
    }
    setBluetoothLocalName(name: string) {
        muzik.setBluetoothLocalName(name);
    }
    getAutoPlaySetting() {
        //TODO
    }
    getChargeStatus() {
        muzik.getChargeStatus(function (data) { alert(data) });
    }
    getSerialNumber() {
        muzik.getSerialNumber(function (data) { alert(data) });
    }
    getManufacturer() {
        muzik.getManufacturer(function (data) { alert(data) });
    }
    getFirmwareVersion() {
        muzik.getFirmwareVersion(function (data) { alert(data) });
    }
    getHardwareVersion() {
        muzik.getHardwareVersion(function (data) { alert(data) });
    }
    getMspVersion() {
        muzik.getMspVersion(function (data) { alert(data) });
    }
    getLibraryVersion() {
        muzik.getLibraryVersion(function (data) { alert(data) });
    }
   
}