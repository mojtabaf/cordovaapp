"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var Rx_1 = require('rxjs/Rx');
var PluginService = (function () {
    function PluginService() {
    }
    PluginService.prototype.startServer = function () {
        muzik.startServer();
    };
    PluginService.prototype.stopServer = function () {
        muzik.stopServer();
    };
    PluginService.prototype.registerForConnectionState = function (callback) {
        muzik.registerForConnectionState(callback);
    };
    PluginService.prototype.observableStartServer = function () {
        var server = this;
        var start = Rx_1.Observable.create(function (observer) {
            server.startServer();
            observer.onCompleted();
        });
        var connectionState = Rx_1.Observable.create(function (observer) {
            muzik.registerForConnectionState(function (data) {
                observer.onNext(data);
                observer.onCompleted();
            });
        });
        start.concat(connectionState).subscribe(function (data) { alert(data); });
    };
    PluginService.prototype.observableConnectionState = function () {
        // var server = this;
    };
    PluginService.prototype.registerForGestures = function (callback) {
        var gestures = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            gestures[_i - 1] = arguments[_i];
        }
        // var cb = function () { };
        muzik.registerForGestures(callback, gestures);
    };
    PluginService.prototype.registerForAllGestures = function (callback) {
        muzik.registerForAllGestures(callback, true);
        //this.startServer().then(() => {
        //    var result = muzik.registerForConnectionState(function (connectionState) {
        //        if (connectionState === "CONNECTED") {
        //            muzik.registerForAllGestures(callback, true);
        //        }
        //    })
        //});
    };
    PluginService.prototype.unregisterForGestures = function () {
        muzik.registerForConnectionState(function (connectionState) {
            if (connectionState === "CONNECTED") {
                muzik.registerForAllGestures(true);
            }
        });
    };
    PluginService.prototype.unregisterForAllGestures = function () {
        muzik.unregisterForAllGestures(true);
    };
    PluginService.prototype.registerForMotion = function () {
        //TODO
    };
    PluginService.prototype.unregisterForMotion = function () {
        //TODO
    };
    PluginService.prototype.getAccelerometerSample = function () {
        //TODO
    };
    PluginService.prototype.registerForAccelerometerDataStream = function () {
        //TODO
    };
    PluginService.prototype.unregisterForAccelerometerDataStream = function () {
        //TODO
    };
    PluginService.prototype.getBatteryLevel = function () {
        // meghdare battry goshio mide 
        muzik.getBatteryLevel(function (data) { alert(data); });
    };
    PluginService.prototype.getBluetoothLocalName = function () {
        //esme bluethooth name marboot be headphono mide
        muzik.getBluetoothLocalName(function (data) { alert(data); });
    };
    PluginService.prototype.setBluetoothLocalName = function (name) {
        muzik.setBluetoothLocalName(name);
    };
    PluginService.prototype.getAutoPlaySetting = function () {
        //TODO
    };
    PluginService.prototype.getChargeStatus = function () {
        muzik.getChargeStatus(function (data) { alert(data); });
    };
    PluginService.prototype.getSerialNumber = function () {
        muzik.getSerialNumber(function (data) { alert(data); });
    };
    PluginService.prototype.getManufacturer = function () {
        muzik.getManufacturer(function (data) { alert(data); });
    };
    PluginService.prototype.getFirmwareVersion = function () {
        muzik.getFirmwareVersion(function (data) { alert(data); });
    };
    PluginService.prototype.getHardwareVersion = function () {
        muzik.getHardwareVersion(function (data) { alert(data); });
    };
    PluginService.prototype.getMspVersion = function () {
        muzik.getMspVersion(function (data) { alert(data); });
    };
    PluginService.prototype.getLibraryVersion = function () {
        muzik.getLibraryVersion(function (data) { alert(data); });
    };
    PluginService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], PluginService);
    return PluginService;
}());
exports.PluginService = PluginService;
