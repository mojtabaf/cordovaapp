﻿import {Injectable} from 'angular2/core'
import {Http, Response} from 'angular2/http';
import {Track} from '../model/track';

@Injectable()
export class TrackService {
    constructor(private http: Http) {

    }
    getTracks(userId: number) {
        return this.http.get("http://api.soundcloud.com/users/" + userId + "/tracks?client_id=7b98197aa405d7721e64fd3fb5b5eb5a").map((response: Response) => <Track[]>response.json());
    }
}