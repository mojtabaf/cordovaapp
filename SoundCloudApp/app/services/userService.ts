﻿import {Injectable} from 'angular2/core'
import {Http, Response} from 'angular2/http';
import {User} from '../model/user';

@Injectable()
export class UserService {
    user: User;
    constructor(private http: Http) {
        
    }
    getUserInfo() {
        return this.http.get("https://api.soundcloud.com/resolve?url=https%3A//soundcloud.com/spadgos&client_id=7b98197aa405d7721e64fd3fb5b5eb5a").map((res: Response) => <User>res.json());//.subscribe(res => this.user = res);
       
    }
}