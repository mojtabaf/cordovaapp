﻿
export class User {
    id: number;
    permalink: string;
    username: string;
    uri: string;
    permalink_url: string;
    avatar_url: string;
}

export class CreatedWith {
    id: number;
    name: string;
    uri: string;
    permalink_url: string;
}

export class Track {
    id: number;
    created_at: string;
    user_id: number;
    duration: number;
    commentable: boolean;
    state: string;
    sharing: string;
    tag_list: string;
    permalink: string;
    description: any;
    streamable: boolean;
    downloadable: boolean;
    genre: any;
    release: any;
    purchase_url: any;
    label_id: any;
    label_name: any;
    isrc: any;
    video_url: any;
    track_type: string;
    key_signature: any;
    bpm: any;
    title: string;
    release_year: any;
    release_month: any;
    release_day: any;
    original_format: string;
    original_content_size: number;
    license: string;
    uri: string;
    permalink_url: string;
    artwork_url: any;
    waveform_url: string;
    user: User;
    stream_url: string;
    download_url: string;
    playback_count: number;
    download_count: number;
    favoritings_count: number;
    comment_count: number;
    created_with: CreatedWith;
    attachments_uri: string;
}