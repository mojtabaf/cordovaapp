﻿export class Product {
        id: string;
        name: string;
    }

export class Subscription {
        product: Product;
    }

export class User {
    id: number;
    kind: string;
    permalink: string;
    username: string;
    last_modified: string;
    uri: string;
    permalink_url: string;
    avatar_url: string;
    country: string;
    first_name: string;
    last_name: string;
    full_name: string;
    description: string;
    city: string;
    discogs_name: any;
    myspace_name: any;
    website: string;
    website_title: string;
    online: boolean;
    track_count: number;
    playlist_count: number;
    plan: string;
    public_favorites_count: number;
    subscriptions: Subscription[];
    followers_count: number;
    followings_count: number;
}